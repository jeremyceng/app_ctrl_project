# -*- coding: utf-8 -*-

# 导入模块 ####################################################################
import subprocess # 用于手机ADB控制

import random # 随机数模块

import time # 用于延时

import datetime

import aircv as ac # 用于发现相似的图的位置

import sys

import os

from PIL import Image

import urllib2

import re

###############################################################################
DEBUG_LOG_FILE_NAME = os.getcwd() + "\\" + "debug_log.txt"

def print_debug_log(str_debug_log): 
    print str_debug_log
   
    fp = open(DEBUG_LOG_FILE_NAME, "a+") # 以写方式打开文件，如果不存在文件，则创建
    fp.write(str_debug_log + "\r\n")
    fp.close()
    
def debug_log_file_clean():
    if True == os.path.exists(DEBUG_LOG_FILE_NAME):
        os.remove(DEBUG_LOG_FILE_NAME) 
    
###############################################################################
ADB_CMD_CHECK_DEVICES = r"adb devices" # 命令：查看已连接的设备列表
ADB_CMD_START_APP = r"shell am start -n " # 命令：启动某个APP
ADB_CMD_GET_PACKAGE_NAME = r"shell dumpsys package > ./package.txt" # 命令：获取APP包名
ADB_CMD_SCREENSHOT = r"shell /system/bin/screencap -p /sdcard/im_tmp_screenshot.png" # 命令：在手机上截图，并保存图片
ADB_CMD_SCREENSHOT_SAVE = r"pull /sdcard/im_tmp_screenshot.png im_tmp_screenshot.png" # 命令：将图片保存到电脑
ADB_CMD_KEYEVENT_HOME = r"shell input keyevent KEYCODE_HOME" # 命令：点击<HOME>键
ADB_CMD_KEYEVENT_BACK = r"shell input keyevent KEYCODE_BACK" # 命令：点击<BACK>键
ADB_CMD_KEYEVENT_NUM = r"shell input keyevent " # 命令：点击<数字键>键
ADB_CMD_SWIPE = r"shell input swipe " # 命令：滑动屏幕
ADB_CMD_TAP = r"shell input tap " # 命令：点击屏幕
ADB_CMD_KEYCODE_0 = 7

###############################################################################
def __adb_cmd(cmd): # 执行手机控制指令
    stdout, stderr = (subprocess.Popen(cmd, stderr = subprocess.PIPE, stdout = subprocess.PIPE, shell = True)).communicate() # 执行adb命令，并返回执行结果
    
    #输出执行命令结果
    stdout = stdout.decode("utf-8")
    stderr = stderr.decode("utf-8")
        
    return stdout    
###############################################################################
def __adb_find_devices(): # 查询设备   
    str_find_devices = __adb_cmd(ADB_CMD_CHECK_DEVICES)
    
    devices_num = 0        
    serial_nos = []
    for item in str_find_devices.split():
        filters = ['list', 'of', 'device', 'devices', 'attached']
        if item.lower() not in filters:
            serial_nos.append(item)
            devices_num = devices_num + 1
    
    if devices_num > 0:
        print_debug_log("the find devices num is %d"%devices_num) 
        
        for serial_no in serial_nos:
            print_debug_log("success: serial_no is " + serial_no) 
        return serial_nos
    else:
        print_debug_log("error: not find any devices") 
        return "None"
    
###############################################################################
def adb_find_devices(): # 查询指定的设备   
    list_find_devices = __adb_find_devices()
    
    if "None" != list_find_devices:
        for list_item in list_find_devices:
            if ADB_DEVICES_CODE == list_item:
                return True
        return False
    else:
        return False
    
###############################################################################
def adb_phone_cmd(cmd): # 执行手机控制指令
    cmd = "adb -s %s "%ADB_DEVICES_CODE + cmd
    
    stdout = __adb_cmd(cmd)
        
    return stdout  
    
###############################################################################
def adb_screenshot(): # 手机截屏       
    adb_phone_cmd(ADB_CMD_SCREENSHOT)
    adb_phone_cmd(ADB_CMD_SCREENSHOT_SAVE) 
            
###############################################################################
def image_find(im_src_path, im_obj_path): # 在一张图片中匹配一块相同区域  
    im_src = ac.imread(im_src_path)
    im_obj = ac.imread(im_obj_path)
                            
    # {'confidence': 0.9984520673751831, 'result': (745L, 533L), 'rectangle': ((571, 473), (571, 594L), (920L, 473), (920L, 594L))}
    find_result = ac.find_template(im_src, im_obj)
    #print_debug_log(str(find_result))
    
    return find_result        
###############################################################################
SIMILAR_RADIO = 99.99 # 图片相似度
def image_cmp(im_src_path, im_obj_path):
    str_im_tar_pos_x0 = "0"
    str_im_tar_pos_y0 = "0"
    str_im_tar_pos_xy1 = "0"
    str_im_tar_pos_xy2 = "0"
    ret_val = False
    
    str_find_result = image_find(im_src_path, im_obj_path)
    if str_find_result == None:
        print_debug_log("error: not find image")
        ret_val = False
    else:
        confidence = str_find_result['confidence'] * 100
        if confidence < SIMILAR_RADIO:
            print_debug_log("error: not find image, the similarity is %.3f%%"%confidence)
            ret_val = False
        else:                
            print_debug_log("success: find image is ok, the similarity is %.3f%%"%confidence)  
            str_im_tar_pos_x0 = str_find_result['result'][0] # 保存识别到的位置 
            str_im_tar_pos_y0 = str_find_result['result'][1] # 保存识别到的位置  
            str_im_tar_pos_xy1 = str_find_result['rectangle'][0]
            str_im_tar_pos_xy2 = str_find_result['rectangle'][3]
            ret_val = True
    return (ret_val, str_im_tar_pos_x0, str_im_tar_pos_y0, str_im_tar_pos_xy1, str_im_tar_pos_xy2)    
###############################################################################
def image_cut_picture(im_src_path, im_dst_path, pos_x0, pos_y0, pos_x1, pos_y1):
    # step 1: read the image
    im = Image.open(im_src_path)
    
    # step 2: cut the image
    box = (pos_x0, pos_y0, pos_x1, pos_y1)
    region = im.crop(box)
    
    # step3: save the image
    region.save(im_dst_path)  
   
###############################################################################
def find_image_and_tap(im_src_path, im_dst_path):
    ret_find_result = False
    
    state_jump_result, im_tar_pos_x0, im_tar_pos_y0, im_tar_pos_xy1, im_tar_pos_xy2 = image_cmp(im_src_path, im_dst_path)
    if False == state_jump_result:            
        ret_find_result = False
    else:                
        print_debug_log("click the image")
        tap_random_x0 =  im_tar_pos_x0
        tap_random_y0 =  im_tar_pos_y0
        print_debug_log(ADB_CMD_TAP + r"%d %d"%(tap_random_x0, tap_random_y0))
        adb_phone_cmd(ADB_CMD_TAP + r"%d %d"%(tap_random_x0, tap_random_y0))
        time.sleep(5 + random.uniform(0, 1)) 
         
        ret_find_result = True
        
    return ret_find_result

###############################################################################

# 用户参数配置 #################################################################
# 手机分辨率不同时应修改的参数 ##################################################
IM_STAND_HOMEPAGE = "hcgs_im_stand_homepage.png"
IM_STAND_NEXTNEW = "hcgs_im_stand_nextnew.png"
IM_STAND_NOTLOGIN = "hcgs_im_stand_notlogin.png"

# 应用不同时应修改的参数 #######################################################
YH_LOG_TXT_FILE_NAME = "hcgs_yh_log.txt"

# 运行参数 ####################################################################

# 全局变量 ####################################################################
g_end_state = False
g_run_state = 0

g_err_cnt_nofind_app_homepage = 0
g_err_cnt_nofind_next_news = 0

# 程序开始 ####################################################################

DEBUG_ENABLE = False

# 清除debug_log文件
debug_log_file_clean()

# 输入参数 ####################################################################
if True == DEBUG_ENABLE:
    # 模拟输入参数    
    input_par_1 = "c76290017d28"
    input_par_2 = "1"
    input_par_3 = "20"
else:
    # 获取输入参数
    input_argv_num = len(sys.argv) # 获取输入参数个数
    if 4 == input_argv_num:
        g_end_state = False
        input_par_1 = sys.argv[1]
        input_par_2 = sys.argv[2]
        input_par_3 = sys.argv[3]
    else:
        g_end_state = True
        print_debug_log("the input parameter is error")
    
# 输入参数处理   
ADB_DEVICES_CODE = input_par_1
if ("" == ADB_DEVICES_CODE):
    g_end_state = True
    print_debug_log("the input parameter is error")
    
g_read_num_max = int(input_par_2)
if ((g_read_num_max < 0) or (1000 < g_read_num_max)):
    g_end_state = True
    print_debug_log("the input parameter is error")
        
g_read_time_max = int(input_par_3)
if ((g_read_time_max < 0) or (1000 < g_read_time_max)):
    g_end_state = True
    print_debug_log("the input parameter is error")

###############################################################################

# 输出参数 ####################################################################
output_par_1 = 0
output_par_2 = 0
        
# 输出参数处理
g_exec_result_state = output_par_1
g_read_num_finished = output_par_2
        
###############################################################################

print_debug_log("the python program is start")

start_time = datetime.datetime.now()
print_debug_log("the start time is %s"%str(start_time))

while False == g_end_state:
    if 0 == g_run_state: 
        print_debug_log("goto running state is %d"%g_run_state)
        time.sleep(0 + random.uniform(0, 1))
        
        print_debug_log("connect phone")
        find_result = adb_find_devices()
        time.sleep(3 + random.uniform(0, 1))
        if False == find_result:  
            print_debug_log("error: not find the devices")                          
            g_end_state = True
        else:
            print_debug_log("success: find the devices")
            g_run_state = g_run_state + 1 
    elif 1 == g_run_state:   
        print_debug_log("goto running state is %d"%g_run_state)
        time.sleep(0 + random.uniform(0, 1))
            
        adb_screenshot()        
        state_jump_result, im_tar_pos_x0, im_tar_pos_y0, im_tar_pos_xy1, im_tar_pos_xy2 = image_cmp("im_tmp_screenshot.png", IM_STAND_HOMEPAGE)
        if False == state_jump_result:          
            g_err_cnt_nofind_app_homepage = g_err_cnt_nofind_app_homepage + 1
            if g_err_cnt_nofind_app_homepage >= 5:
                g_end_state = True
            else:
                g_run_state = g_run_state + 1
        else:         
            g_err_cnt_nofind_app_homepage = 0
			
            g_run_state = 3    
    elif 2 == g_run_state:
        print_debug_log("goto running state is %d"%g_run_state)
        time.sleep(0 + random.uniform(0, 1))
        
        print_debug_log("click <BACK>")
        adb_phone_cmd(ADB_CMD_KEYEVENT_BACK)
        time.sleep(2 + random.uniform(0, 1))
        
        g_run_state = 1
    elif 3 == g_run_state:
        print_debug_log("goto running state is %d"%g_run_state)
        time.sleep(0 + random.uniform(0, 1))
                 
        for i in range(0, 4):     
            swipe_random_x0 =  300 + random.randint(10, 30)
            swipe_random_y0 =  800 + random.randint(10, 30)
            swipe_random_x1 =  400 + random.randint(10, 30) 
            swipe_random_y1 =  swipe_random_y0 - 300                           
            swipe_random_time =  300 + random.randint(0, 200)
            adb_phone_cmd(ADB_CMD_SWIPE + r"%d %d %d %d %d"%(swipe_random_x0, swipe_random_y0, swipe_random_x1, swipe_random_y1, swipe_random_time))
        
        time.sleep(1 + random.uniform(0, 1))
        
        g_run_state = g_run_state + 1
    elif 4 == g_run_state:
        print_debug_log("goto running state is %d"%g_run_state)
        time.sleep(0 + random.uniform(0, 1))
        
        adb_screenshot()
        state_jump_result, im_tar_pos_x0, im_tar_pos_y0, im_tar_pos_xy1, im_tar_pos_xy2 = image_cmp("im_tmp_screenshot.png", IM_STAND_NEXTNEW)
        if False == state_jump_result:            
            g_err_cnt_nofind_next_news = g_err_cnt_nofind_next_news + 1
            if g_err_cnt_nofind_next_news >= 5:
                g_end_state = True
            else:                
                g_run_state = 2 
        else:
            g_err_cnt_nofind_next_news = 0
			
            g_run_state = g_run_state + 1  
    elif 5 == g_run_state:
        print_debug_log("goto running state is %d"%g_run_state)
        time.sleep(0 + random.uniform(0, 1))
                                         
        pos_err = im_tar_pos_y0 - 700            
        if pos_err > 0:                            
            tmp_cmd_pos = ADB_CMD_SWIPE + r"300 1200 400 %d 2000"%(1200 - pos_err)
        else:                            
            tmp_cmd_pos = ADB_CMD_SWIPE + r"300 400 400 %d 2000"%(400 - pos_err)                            
        adb_phone_cmd(tmp_cmd_pos)
        time.sleep(1 + random.uniform(0, 1))
                        
        print_debug_log("click next news")
        tap_random_x0 =  im_tar_pos_x0 + 100 + random.randint(0, 300)
        tap_random_y0 =  700 + 100 + random.randint(0, 200)
        print_debug_log(ADB_CMD_TAP + r"%d %d"%(tap_random_x0, tap_random_y0))
        adb_phone_cmd(ADB_CMD_TAP + r"%d %d"%(tap_random_x0, tap_random_y0)) 
        time.sleep(3 + random.uniform(0, 1))
        
        for i in range(0, g_read_time_max): 
            swipe_random_x0 =  300 + random.randint(10, 30)
            swipe_random_y0 =  800 + random.randint(10, 30)
            swipe_random_x1 =  400 + random.randint(10, 30) 
            swipe_random_y1 =  swipe_random_y0 - 200                           
            swipe_random_time =  300 + random.randint(0, 200)
            adb_phone_cmd(ADB_CMD_SWIPE + r"%d %d %d %d %d"%(swipe_random_x0, swipe_random_y0, swipe_random_x1, swipe_random_y1, swipe_random_time))

        adb_screenshot()        
        state_jump_result, im_tar_pos_x0, im_tar_pos_y0, im_tar_pos_xy1, im_tar_pos_xy2 = image_cmp("im_tmp_screenshot.png", IM_STAND_NOTLOGIN)
        if True == state_jump_result:
            g_end_state = True
        else:
            g_run_state = g_run_state + 1
    elif 6 == g_run_state:
        print_debug_log("goto running state is %d"%g_run_state)
        time.sleep(0 + random.uniform(0, 1))
        
        print_debug_log("click <BACK>")
        adb_phone_cmd(ADB_CMD_KEYEVENT_BACK)
        time.sleep(2 + random.uniform(0, 1))
   
        g_read_num_finished = g_read_num_finished + 1
        print_debug_log("g_read_num_finished = %d"%g_read_num_finished)
        if g_read_num_finished < g_read_num_max:
            g_run_state = 1
        else:
            g_run_state = g_run_state + 1
    elif 7 == g_run_state:
        print_debug_log("goto running state is %d"%g_run_state)
        time.sleep(0 + random.uniform(0, 1))
        
        g_exec_result_state = 255
        
        print_debug_log("success: the program running normal")
        g_end_state = True         
    else:
        print_debug_log("goto running state is %d"%g_run_state)
        time.sleep(0 + random.uniform(0, 1))
        
        print_debug_log("error: the running state is unknown number")
        g_end_state = True

# 记录信息 ##########################################################
if 255 == g_exec_result_state:
    f = file(YH_LOG_TXT_FILE_NAME, "a+")
    f.writelines('\n' + ADB_DEVICES_CODE + " " + str(g_read_num_finished) + " " + str(datetime.datetime.now()))
    f.close()   
            
if 255 != g_exec_result_state:
    g_exec_result_state = g_run_state
stop_time = datetime.datetime.now()  
print_debug_log("the stop time is %s"%str(stop_time))              
print_debug_log("the running time is %s seconds"%((stop_time - start_time).total_seconds()))  
               
print_debug_log("the python program is end")

# 输出参数处理
output_par_1 = g_exec_result_state
output_par_2 = g_read_num_finished
print_debug_log("PYTHON_EXEC_OUPUT %d %d"%(output_par_1, output_par_2))

print_debug_log("PYTHON_EXIT_CODE_0X12345678")

