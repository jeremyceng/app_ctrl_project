
import phone_ctrl
import image_handler
import debug_print
import combination_function
import time
import datetime
import sys
import calendar

class auto_run_param():
	start_time_mon =		0
	start_time_mday = 		0
	start_time_hour =     	0
	start_time_min =     	0

#data_min < data < data_max
def input_data(data_name, data_max, data_min):
	while True:
		s = input("-> input " + data_name + ":")
		if not s.isdigit():
			print("wrong format, must be number")
			continue
		ret_data = int(s)
		if ret_data >= data_max:
			print("must less then ", data_max)
			continue
		elif ret_data <= data_min:
			print("must more then ", data_min)
			continue
		else:
			print(data_name + " --> ", ret_data)
			break
	return ret_data

####################################################################################################################

DING_LOGE = "/ding_loge.png"

"""
:1：激活设备编号（0~n）
:2：手机APP图标分布 行
:3：手机APP图标分布 列
:4 页（home 为0，向右加1）
:5 行（最上一行为0）
:6 个（最左一个为0）
:7 定时启动 月
:8 定时启动 日
:9 定时启动 小时
:10 定时启动 分钟
"""
active_device_num =    	0
app_layout_row_def =   	6
app_layout_column_def =	4
app_page =        		2
app_row =        		0
app_column =     		0

#app_layout_row_def = input_data("app_layout_row_def", 7, 3)
#app_layout_column_def = input_data("app_layout_column_def", 6, 2)
#app_page = input_data("app_page", 10, -1)
#app_row = input_data("app_row", app_layout_row_def, -1)
#app_column = input_data("app_column", app_layout_column_def, -1)

auto_run_count = input_data("auto_run_count", 5, 0)
auto_run_param_s = [auto_run_param() for i in range(auto_run_count)]

i = 0
while True:
	print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
	print("input ", i , "round start time")
	auto_run_param_s[i].start_time_mon = input_data("start_time_mon", 13, 0)
	auto_run_param_s[i].start_time_mday = input_data("start_time_mday", calendar.mdays[auto_run_param_s[i].start_time_mon]+1, 0)
	auto_run_param_s[i].start_time_hour = input_data("start_time_hour", 24, -1)
	auto_run_param_s[i].start_time_min = input_data("start_time_min", 60, -1)
	i = i+1
	if i >= auto_run_count:
		break

combination_function.phone_init(active_device_num, app_layout_row_def, app_layout_column_def)

i = 0
while True:
	print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
	print(i, "round start")
	print("app will auto run at ",\
		auto_run_param_s[i].start_time_mon,\
		auto_run_param_s[i].start_time_mday,\
		auto_run_param_s[i].start_time_hour,\
		auto_run_param_s[i].start_time_min)

	phone_ctrl.phone_ctrl_go_home()
	time.sleep(1)
	phone_ctrl.phone_ctrl_go_home()
	while True:
		localtime = time.localtime(time.time())
		print ("timing... ", localtime.tm_mon, localtime.tm_mday, localtime.tm_hour, localtime.tm_min)
		if localtime.tm_mon == auto_run_param_s[i].start_time_mon and\
		localtime.tm_mday == auto_run_param_s[i].start_time_mday and\
		localtime.tm_hour == auto_run_param_s[i].start_time_hour and\
		localtime.tm_min >= auto_run_param_s[i].start_time_min:
			break
		else:
			time.sleep(60)
	localtime = time.localtime(time.time())
	print("++++++++++++++++++++++++++++++++++++++++++++++++++++++")
	print(i, "round")
	print ("start run at", localtime.tm_mon, localtime.tm_mday, localtime.tm_hour, localtime.tm_min)
	phone_ctrl.phone_ctrl_open_app(app_page, app_row, app_column)
	time.sleep(10)

	############周末加班########
	phone_ctrl.phone_ctrl_tep_xy(359 ,1217) #智能工作助理->打卡
	time.sleep(10)
	phone_ctrl.phone_ctrl_tep_xy(333 ,866)	#打卡界面->打卡
	time.sleep(10)
	phone_ctrl.phone_ctrl_tep_xy(525 ,1406)	#返回智能工作助理
	############周末加班########

	time.sleep(20)
	i = i+1
	if i >= auto_run_count:
		break

phone_ctrl.phone_ctrl_screenshot()
time.sleep(5)
phone_ctrl.phone_ctrl_power_button()

"""
ret = combination_function.find_logo(DING_LOGE, 980)
if ret[0] == 0:
	debug_print.print_debug_log("not find NOTLOGIN")
else:
	phone_ctrl.phone_ctrl_tep_xy(ret[0], ret[1])
	time.sleep(2)
	phone_ctrl.phone_ctrl_screenshot()
"""
