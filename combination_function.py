import phone_ctrl
import image_handler
import debug_print
import time

##########################################################
def find_logo(logo, pencent):
    ret = image_handler.image_find_location("im_tmp_screenshot_" + \
            phone_ctrl.PHONE_CTRL_ACTIVE_DEVICES_CODE_FOR_FILE + ".png", 
            phone_ctrl.PHONE_CTRL_ACTIVE_DEVICES_CODE_FOR_FILE + logo)
    if ret == 0 or ret[2] < pencent:
        debug_print.print_debug_log("log not find " + logo)
        return 0, 0
    else:
        debug_print.print_debug_log("log find OK " + logo)
        return int(ret[0]), int(ret[1])

##########################################################
def phone_init(active_device_num, app_layout_line_def, app_layout_num_def):
    phone_ctrl.phone_ctrl_init_devices_list()
    phone_ctrl.phone_ctrl_init_set_active_device(active_device_num)
    phone_ctrl.phone_ctrl_def_app_line_num(app_layout_line_def, app_layout_num_def)
    phone_ctrl.phone_ctrl_get_sreen_size()
