import  phone_ctrl
import  os

DEBUG_LOG_FILE_NAME = os.getcwd() + "\\" + "temp_debug_log.txt"
#***********************************************************************
def print_debug_log(str_debug_log): 
    print(str_debug_log)
    DEBUG_LOG_FILE_NAME = os.getcwd() + "\\" + phone_ctrl.PHONE_CTRL_ACTIVE_DEVICES_CODE_FOR_FILE + "_debug_log.txt"
    fp = open(DEBUG_LOG_FILE_NAME, "a+") # 以写方式打开文件，如果不存在文件，则创建
    fp.write(str_debug_log + "\r\n")
    fp.close()

#***********************************************************************
def debug_log_file_clean():
    DEBUG_LOG_FILE_NAME = os.getcwd() + "\\" + phone_ctrl.PHONE_CTRL_ACTIVE_DEVICES_CODE_FOR_FILE + "_debug_log.txt"
    if True == os.path.exists(DEBUG_LOG_FILE_NAME):
        os.remove(DEBUG_LOG_FILE_NAME) 