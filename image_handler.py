
import debug_print
import aircv as ac # 用于发现相似的图的位置

################################################################################
#im_src = ac.imread("im_tmp_screenshot.png") #图片位于PY文件相同目录下
#im_obj = ac.imread("im_tmp_screenshot_copy.png")
#find_result = ac.find_template(im_src, im_obj)
#print(str(find_result))
# {'confidence': 0.9984520673751831, 'result': (745L, 533L), 'rectangle': ((571, 473), (571, 594L), (920L, 473), (920L, 594L))}
#confidence:    匹配度 0~1.0
#result：       im_obj的中心点在im_src中的位置（左上角为原点，向右下延伸，坐标格式是（水平，垂直））
#rectangle:     im_obj的四个顶点在im_src中的位置，分别为左上、左下、右上、右下
#################################################################################

#***********************************************************************
def image_find(im_src_path, im_obj_path): # 在一张图片中匹配一块相同区域  
    im_src = ac.imread(im_src_path)
    im_obj = ac.imread(im_obj_path)
    print("find [" + im_obj_path + "] at [" + im_src_path + "]")
    # {'confidence': 0.9984520673751831, 'result': (745L, 533L), 'rectangle': ((571, 473), (571, 594L), (920L, 473), (920L, 594L))}
    find_result = ac.find_template(im_src, im_obj)
    print(str(find_result))
    
    return find_result        

#***********************************************************************
def image_find_location(im_src_path, im_obj_path):
    location = [0, 0]
    find_result = image_find(im_src_path, im_obj_path)
    if find_result != None:
        location = find_result['result']
        print("find ok, the similarity is %f" %find_result['confidence'])
        return int(location[0]), int(location[1]) , int(find_result['confidence']*1000)
    else:
        print("error: not find image")
        return 0