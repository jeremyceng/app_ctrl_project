
ADB_CMD_CHECK_DEVICES = r"adb devices" # 命令：查看已连接的设备列表
ADB_CMD_START_APP = r"shell am start -n " # 命令：启动某个APP
ADB_CMD_GET_PACKAGE_NAME = r"shell dumpsys package > ./package.txt" # 命令：获取APP包名
ADB_CMD_SCREENSHOT = r"shell /system/bin/screencap -p /sdcard/im_tmp_screenshot.png" # 命令：在手机上截图，并保存图片
ADB_CMD_SCREENSHOT_SAVE = r"pull /sdcard/im_tmp_screenshot.png im_tmp_screenshot.png" # 命令：将图片保存到电脑
ADB_CMD_KEYEVENT = r"shell input keyevent"
ADB_CMD_KEYEVENT_HOME = r"shell input keyevent KEYCODE_HOME" # 命令：点击<HOME>键
ADB_CMD_KEYEVENT_BACK = r"shell input keyevent KEYCODE_BACK" # 命令：点击<BACK>键
ADB_CMD_KEYEVENT_NUM = r"shell input keyevent " # 命令：点击<数字键>键
ADB_CMD_SWIPE = r"shell input swipe " # 命令：滑动屏幕
ADB_CMD_TAP = r"shell input tap " # 命令：点击屏幕

APP_LAYOUT_LINE = 5 #5行 - 80~100  + 1590
APP_LAYOUT_NUM = 4  #4列S
PHONE_CTRL_PAGE_HEADER_PERCENT = 0.0005
PHONE_CTRL_PAGE_FOOTER_PERCENT = 0.15
PHONE_CTRL_PAGE_HEADER = 400
PHONE_CTRL_PAGE_FOOTER = 300
PHONE_CTRL_SCREEN_WIDE = 0
PHONE_CTRL_SCREEN_WIDE = 0
PHONE_CTRL_SCREEN_WIDE = 0
PHONE_CTRL_ACTIVE_DEVICES_CODE = "x"
PHONE_CTRL_ACTIVE_DEVICES_CODE_FOR_FILE = "x"
PHONE_CTRL_ACTIVE_DEVICES_LISTS = []

import debug_print
import time
import subprocess # 用于手机ADB控制

########################################################################
def __adb_cmd(cmd): # 执行手机控制指令
    stdout, stderr = (subprocess.Popen(cmd, stderr = subprocess.PIPE,
                    stdout = subprocess.PIPE, shell = True)).communicate() # 执行adb命令，并返回执行结果

    print("*****************************************")
    print("adb send:\r\n" + cmd)
    print("-----------------------------------------")
    #输出执行命令结果
    stdout = stdout.decode("utf-8")
    stderr = stderr.decode("utf-8")
    print("-----------------------------------------")
    print("adb rec out:\r\n" + stdout)
    print("-----------------------------------------")
    print("adb rec err:\r\n" + stderr)
    print("=========================================")

    return stdout


def adb_phone_cmd(cmd): # 执行手机控制指令
    cmd = "adb -s " + PHONE_CTRL_ACTIVE_DEVICES_CODE + ' ' + cmd
    stdout = __adb_cmd(cmd)

#    cmd = "adb " + cmd
#    stdout = os.popen(cmd)
    return stdout
########################################################################

#***********************************************************************
def phone_ctrl_init_devices_list():
    str_find_devices = __adb_cmd(ADB_CMD_CHECK_DEVICES)
    global PHONE_CTRL_ACTIVE_DEVICES_LISTS
    devices_num = 0
    for item in str_find_devices.split():
        filters = ['list', 'of', 'device', 'devices', 'attached']
        if item.lower() not in filters:
            PHONE_CTRL_ACTIVE_DEVICES_LISTS.append(item)
            devices_num = devices_num + 1

    if devices_num > 0:
        print("the find devices num is %d"%devices_num)

        for PHONE_CTRL_ACTIVE_DEVICES_LISTS_OBJ in PHONE_CTRL_ACTIVE_DEVICES_LISTS:
            print("success: serial_no is " + PHONE_CTRL_ACTIVE_DEVICES_LISTS_OBJ)

        return PHONE_CTRL_ACTIVE_DEVICES_LISTS_OBJ
    else:
        print("error: not find any devices")
        return "None"

#***********************************************************************
def phone_ctrl_init_set_active_device(devices_num):
    global PHONE_CTRL_ACTIVE_DEVICES_CODE
    global PHONE_CTRL_ACTIVE_DEVICES_CODE_FOR_FILE
    PHONE_CTRL_ACTIVE_DEVICES_CODE = str(PHONE_CTRL_ACTIVE_DEVICES_LISTS[devices_num])
    PHONE_CTRL_ACTIVE_DEVICES_CODE_FOR_FILE = PHONE_CTRL_ACTIVE_DEVICES_CODE.replace('.', '')
    PHONE_CTRL_ACTIVE_DEVICES_CODE_FOR_FILE = PHONE_CTRL_ACTIVE_DEVICES_CODE_FOR_FILE.replace(':', '')
    print("phone_ctrl_init_set_active_device\r\nnum:%d\tcode:"
                                %devices_num + PHONE_CTRL_ACTIVE_DEVICES_CODE)

#***********************************************************************
def phone_ctrl_get_sreen_size():
    global PHONE_CTRL_SCREEN_WIDE, PHONE_CTRL_SCREEN_HIGHT, PHONE_CTRL_INIT_FALG
    s = adb_phone_cmd("shell wm size")
    s = s[15:-1]
    pos = s.find('x')
#    print("POS ", pos)
    PHONE_CTRL_SCREEN_WIDE = int(s[0:pos])
    PHONE_CTRL_SCREEN_HIGHT = int(s[pos+1:-1])
    print("phone_ctrl_init")
    print("PHONE_CTRL_SCREEN_WIDE: %s" %PHONE_CTRL_SCREEN_WIDE)
    print("PHONE_CTRL_SCREEN_HIGHT: %s" %PHONE_CTRL_SCREEN_HIGHT)
    PHONE_CTRL_INIT_FALG = 1

#***********************************************************************
def phone_ctrl_key_event(key_event):
    adb_phone_cmd(ADB_CMD_KEYEVENT + ' ' + str(int(key_event)))
    print("phone_ctrl_key_event" + ' ' + str(int(key_event)))

#***********************************************************************
def phone_ctrl_key_up():
    phone_ctrl_key_event(19)

#***********************************************************************
def phone_ctrl_key_down():
    phone_ctrl_key_event(20)

#***********************************************************************
def phone_ctrl_power_button():
    phone_ctrl_key_event(26)
    print("phone_ctrl_power_button")

#***********************************************************************
def phone_ctrl_go_home():
    adb_phone_cmd(ADB_CMD_KEYEVENT_HOME)
    print("phone_ctrl_go_home")

#***********************************************************************
def phone_ctrl_go_back():
    adb_phone_cmd(ADB_CMD_KEYEVENT_BACK)
    print("phone_ctrl_go_back")


#***********************************************************************
def phone_ctrl_swipe_right():
    if PHONE_CTRL_SCREEN_WIDE == 0 or PHONE_CTRL_SCREEN_HIGHT == 0:
        phone_ctrl_get_sreen_size()
    s = ADB_CMD_SWIPE + str(int(PHONE_CTRL_SCREEN_WIDE/4)) + ' ' + str(int(PHONE_CTRL_SCREEN_HIGHT/2)) + \
        ' ' + str(int(3*PHONE_CTRL_SCREEN_WIDE/4)) + ' ' + str(int(PHONE_CTRL_SCREEN_HIGHT/2))

    print("phone_ctrl_swipe_right")
    adb_phone_cmd(s)

#***********************************************************************
def phone_ctrl_swipe_left():
    if PHONE_CTRL_SCREEN_WIDE == 0 or PHONE_CTRL_SCREEN_HIGHT == 0:
        phone_ctrl_get_sreen_size()
    s = ADB_CMD_SWIPE + str(int(3*PHONE_CTRL_SCREEN_WIDE/4)) + ' ' + str(int(PHONE_CTRL_SCREEN_HIGHT/2)) + \
        ' ' + str(int(PHONE_CTRL_SCREEN_WIDE/4)) + ' ' + str(int(PHONE_CTRL_SCREEN_HIGHT/2))

    print("phone_ctrl_swipe_left")
    adb_phone_cmd(s)

#***********************************************************************
def phone_ctrl_tep_xy(x, y):
    s = ADB_CMD_TAP + str(int(x)) + ' ' + str(int(y))
    print("phone_ctrl_tep_xy at " + str(x) + ' ' + str(y))
    adb_phone_cmd(s)

#***********************************************************************
def phone_ctrl_def_app_line_num(line, num):
    global APP_LAYOUT_LINE, APP_LAYOUT_NUM
    APP_LAYOUT_LINE = line
    APP_LAYOUT_NUM = num

#***********************************************************************
#页（home 为0，向右加1） 行（最上一行为0） 个（最左一个为0）
def phone_ctrl_open_app(page, line, num):
    print("phone_ctrl_open_app at " + str(page) + ' ' + str(line) + ' ' + str(num))
    phone_ctrl_go_home()
    time.sleep(0.25)
    phone_ctrl_go_home()
    time.sleep(0.25)
    phone_ctrl_go_home()
    time.sleep(0.25)
    while page > 0:
        phone_ctrl_swipe_left()
        time.sleep(1)
        page -= 1
    x = PHONE_CTRL_SCREEN_WIDE/(2*APP_LAYOUT_NUM + APP_LAYOUT_NUM + 1)*(3*num+2)
    y = (PHONE_CTRL_SCREEN_HIGHT - PHONE_CTRL_SCREEN_HIGHT*PHONE_CTRL_PAGE_FOOTER_PERCENT - \
        PHONE_CTRL_SCREEN_HIGHT*PHONE_CTRL_PAGE_HEADER_PERCENT)/(APP_LAYOUT_LINE + 1)*(line+1) + \
        PHONE_CTRL_SCREEN_HIGHT*PHONE_CTRL_PAGE_HEADER_PERCENT

    phone_ctrl_tep_xy(x ,y)

#***********************************************************************
def phone_ctrl_swipe_up(range):
    if PHONE_CTRL_SCREEN_WIDE == 0 or PHONE_CTRL_SCREEN_HIGHT == 0:
        phone_ctrl_get_sreen_size()
    if(range > PHONE_CTRL_SCREEN_HIGHT - (PHONE_CTRL_PAGE_HEADER + PHONE_CTRL_PAGE_FOOTER)):
        print("range too far")
        return
    s = ADB_CMD_SWIPE + str(int(PHONE_CTRL_SCREEN_WIDE/2)) + ' ' + str(PHONE_CTRL_SCREEN_HIGHT - PHONE_CTRL_PAGE_FOOTER) + \
        ' ' + str(int(PHONE_CTRL_SCREEN_WIDE/2)) + ' ' + str(PHONE_CTRL_SCREEN_HIGHT - PHONE_CTRL_PAGE_FOOTER - range)
    print("phone_ctrl_swipe_up %d" %range)
    adb_phone_cmd(s)

#***********************************************************************
def phone_ctrl_swipe_down(range):
    if PHONE_CTRL_SCREEN_WIDE == 0 or PHONE_CTRL_SCREEN_HIGHT == 0:
        phone_ctrl_get_sreen_size()
    if(range > PHONE_CTRL_SCREEN_HIGHT - (PHONE_CTRL_PAGE_HEADER + PHONE_CTRL_PAGE_FOOTER)):
        print("range too far")
        return
    s = ADB_CMD_SWIPE + str(int(PHONE_CTRL_SCREEN_WIDE/2)) + ' ' + str(PHONE_CTRL_PAGE_HEADER) + ' ' + \
        str(int(PHONE_CTRL_SCREEN_WIDE/2)) + ' ' + str(PHONE_CTRL_PAGE_HEADER + range)
    print("phone_ctrl_swipe_down %d" %range)
    adb_phone_cmd(s)

#***********************************************************************
def phone_ctrl_swipe_up_percent(percent):
    if percent > 100:
        print("percent > 100")
        return
    range = percent*(PHONE_CTRL_SCREEN_HIGHT - (PHONE_CTRL_PAGE_HEADER + PHONE_CTRL_PAGE_FOOTER))/100
    phone_ctrl_swipe_up(int(range))

#***********************************************************************
def phone_ctrl_swipe_down_percent(percent):
    if percent > 100:
        print("percent > 100")
        return
    range = percent*(PHONE_CTRL_SCREEN_HIGHT - (PHONE_CTRL_PAGE_HEADER + PHONE_CTRL_PAGE_FOOTER))/100
    phone_ctrl_swipe_down(int(range))

#***********************************************************************
def phone_ctrl_screenshot(): # 手机截屏
    print("phone_ctrl_screenshot")
    adb_phone_cmd(ADB_CMD_SCREENSHOT)
    s = "pull /sdcard/im_tmp_screenshot.png im_tmp_screenshot_" + PHONE_CTRL_ACTIVE_DEVICES_CODE_FOR_FILE + ".png"
    adb_phone_cmd(s)


"""
0 -->  "KEYCODE_UNKNOWN"
1 -->  "KEYCODE_MENU"
2 -->  "KEYCODE_SOFT_RIGHT"
3 -->  "KEYCODE_HOME"
4 -->  "KEYCODE_BACK"
5 -->  "KEYCODE_CALL"
6 -->  "KEYCODE_ENDCALL"
7 -->  "KEYCODE_0"
8 -->  "KEYCODE_1"
9 -->  "KEYCODE_2"
10 -->  "KEYCODE_3"
11 -->  "KEYCODE_4"
12 -->  "KEYCODE_5"
13 -->  "KEYCODE_6"
14 -->  "KEYCODE_7"
15 -->  "KEYCODE_8"
16 -->  "KEYCODE_9"
17 -->  "KEYCODE_STAR"
18 -->  "KEYCODE_POUND"
19 -->  "KEYCODE_DPAD_UP"
20 -->  "KEYCODE_DPAD_DOWN"
21 -->  "KEYCODE_DPAD_LEFT"
22 -->  "KEYCODE_DPAD_RIGHT"
23 -->  "KEYCODE_DPAD_CENTER"
24 -->  "KEYCODE_VOLUME_UP"
25 -->  "KEYCODE_VOLUME_DOWN"
26 -->  "KEYCODE_POWER"
27 -->  "KEYCODE_CAMERA"
28 -->  "KEYCODE_CLEAR"
29 -->  "KEYCODE_A"
30 -->  "KEYCODE_B"
31 -->  "KEYCODE_C"
32 -->  "KEYCODE_D"
33 -->  "KEYCODE_E"
34 -->  "KEYCODE_F"
35 -->  "KEYCODE_G"
36 -->  "KEYCODE_H"
37 -->  "KEYCODE_I"
38 -->  "KEYCODE_J"
39 -->  "KEYCODE_K"
40 -->  "KEYCODE_L"
41 -->  "KEYCODE_M"
42 -->  "KEYCODE_N"
43 -->  "KEYCODE_O"
44 -->  "KEYCODE_P"
45 -->  "KEYCODE_Q"
46 -->  "KEYCODE_R"
47 -->  "KEYCODE_S"
48 -->  "KEYCODE_T"
49 -->  "KEYCODE_U"
50 -->  "KEYCODE_V"
51 -->  "KEYCODE_W"
52 -->  "KEYCODE_X"
53 -->  "KEYCODE_Y"
54 -->  "KEYCODE_Z"
55 -->  "KEYCODE_COMMA"
56 -->  "KEYCODE_PERIOD"
57 -->  "KEYCODE_ALT_LEFT"
58 -->  "KEYCODE_ALT_RIGHT"
59 -->  "KEYCODE_SHIFT_LEFT"
60 -->  "KEYCODE_SHIFT_RIGHT"
61 -->  "KEYCODE_TAB"
62 -->  "KEYCODE_SPACE"
63 -->  "KEYCODE_SYM"
64 -->  "KEYCODE_EXPLORER"
65 -->  "KEYCODE_ENVELOPE"
66 -->  "KEYCODE_ENTER"
67 -->  "KEYCODE_DEL"
68 -->  "KEYCODE_GRAVE"
69 -->  "KEYCODE_MINUS"
70 -->  "KEYCODE_EQUALS"
71 -->  "KEYCODE_LEFT_BRACKET"
72 -->  "KEYCODE_RIGHT_BRACKET"
73 -->  "KEYCODE_BACKSLASH"
74 -->  "KEYCODE_SEMICOLON"
75 -->  "KEYCODE_APOSTROPHE"
76 -->  "KEYCODE_SLASH"
77 -->  "KEYCODE_AT"
78 -->  "KEYCODE_NUM"
79 -->  "KEYCODE_HEADSETHOOK"
80 -->  "KEYCODE_FOCUS"
81 -->  "KEYCODE_PLUS"
82 -->  "KEYCODE_MENU"
83 -->  "KEYCODE_NOTIFICATION"
84 -->  "KEYCODE_SEARCH"
85 -->  "TAG_LAST_KEYCODE"
"""