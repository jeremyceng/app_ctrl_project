
import phone_ctrl
import image_handler
import debug_print
import combination_function
import time
import datetime
import sys


HCGS_HOME_LOGO = "/hcgs_im_home_logo.png"
HCGS_HOME_AD_LOGO = "/hcgs_im_home_ad.png"
HCGS_ARTICLE_INDEX_LOGO = "/hcgs_im_article_index_logo.png"
HCGS_ARTICLE_LOGO = "/hcgs_im_article_logo.png"

HCGS_HOME_NOTLOGIN_LOGE = "/hcgs_home_notlogin_loge.png"
HCGS_ARTICLE_READ_FINISH_LOGO = "/hcgs_im_article_read_finish_logo.png"
HCGS_GOTO_ARTICLE_LOGO = "/hcgs_im_goto_article_logo.png"
HCGS_AWARD_LOGO = "/hcgs_im_award_logo.png"

#***********************************************************************
#输出数据
read_count = 0
read_time_totality = 0
#***********************************************************************
def hcgs_app_init():
    """
    ok = False
    ad_count = 0
    while ok == False:
        phone_ctrl.phone_ctrl_screenshot()
        ret = combination_function.find_logo(HCGS_HOME_AD_LOGO, 980)
        if ret[0] == 0:
            debug_print.print_debug_log("hcgs_app_init not find ad")
            ok = True
        else:
            debug_print.print_debug_log("hcgs_app_init find ad")
            phone_ctrl.phone_ctrl_go_back()
            time.sleep(1)
            ad_count += 1
            if ad_count > 5:
                ok = True
    """
    phone_ctrl.phone_ctrl_screenshot()
    ret = combination_function.find_logo(HCGS_HOME_NOTLOGIN_LOGE, 980)
    if ret[0] == 0:
        debug_print.print_debug_log("hcgs_app_init not find NOTLOGIN")
    else:
        phone_ctrl.phone_ctrl_tep_xy(ret[0], ret[1])
        time.sleep(2)
        phone_ctrl.phone_ctrl_screenshot()

    ret = combination_function.find_logo(HCGS_GOTO_ARTICLE_LOGO, 980)
    if ret[0] == 0:
        debug_print.print_debug_log("not find GOTO artilce")
    else:
        debug_print.print_debug_log("find GOTO artilce")
        phone_ctrl.phone_ctrl_tep_xy(ret[0], ret[1])
        time.sleep(2)
        
    ok = False
    i = 0
    while ok == False:
        phone_ctrl.phone_ctrl_screenshot()
        ret = combination_function.find_logo(HCGS_HOME_LOGO, 980)
        if ret[0] == 0:
            debug_print.print_debug_log("hcgs_home_page_gotot_article_page not at home page")
            phone_ctrl.phone_ctrl_go_back()
            time.sleep(2)
            i += 1
            if i > 3:
                ok = True
        else:
            i = 0
            ok = True
    if i == 0:
        return 1
    else:
        return 0


#***********************************************************************
def hcgs_home_page_gotot_article_page():
    phone_ctrl.phone_ctrl_screenshot()
    ret = combination_function.find_logo(HCGS_HOME_LOGO, 980)
    if ret[0] == 0:
        debug_print.print_debug_log("hcgs_home_page_gotot_article_page not at home page")
        return 0
    else:
        debug_print.print_debug_log("dftt_home_page_gotot_article_page is at home")

    ret = combination_function.find_logo(HCGS_AWARD_LOGO, 980)
    if ret[0] == 0:
        debug_print.print_debug_log("dftt_article_page_goback_home_page not find award")
    else:
        phone_ctrl.phone_ctrl_tep_xy(ret[0], ret[1])
        time.sleep(3)

    """
    ret = combination_function.find_logo(HCGS_HOME_AD_LOGO, 980)
    if ret[0] == 0:
        debug_print.print_debug_log("hcgs_app_init not find ad")
    else:
        debug_print.print_debug_log("hcgs_app_init find ad")
        phone_ctrl.phone_ctrl_go_back()
        time.sleep(1)
    """

    i = 0
    ok = False
    while ok == False:
        phone_ctrl.phone_ctrl_swipe_up_percent(80)
        time.sleep(1)
        phone_ctrl.phone_ctrl_screenshot()
        ret = combination_function.find_logo(HCGS_ARTICLE_INDEX_LOGO, 980)
        if ret[0] == 0:
            debug_print.print_debug_log("qtt_home_page_gotot_article_page not find article")
            i += 1
            if i > 10:
                ok = True
        else:
            debug_print.print_debug_log("hcgs_home_page_gotot_article_page find article ok")
            debug_print.print_debug_log("at " + str(ret[0]) + ' ' + str(ret[1]))
            phone_ctrl.phone_ctrl_tep_xy(phone_ctrl.PHONE_CTRL_SCREEN_WIDE/2, ret[1] + 100)
            ok = True
            i = 0

    if i == 0:
        #y_err = ret[1] - (phone_ctrl.PHONE_CTRL_SCREEN_HIGHT/2)
        #if y_err > 0: 
        #    phone_ctrl.phone_ctrl_swipe_up(int(y_err))
        #else: 
        #    phone_ctrl.phone_ctrl_swipe_down(int(abs(y_err)))
        #time.sleep(2)
        #phone_ctrl.phone_ctrl_tep_xy(phone_ctrl.PHONE_CTRL_SCREEN_WIDE/2, phone_ctrl.PHONE_CTRL_SCREEN_HIGHT/2+100)
        #time.sleep(0.5)
        #phone_ctrl.phone_ctrl_tep_xy(phone_ctrl.PHONE_CTRL_SCREEN_WIDE/2, phone_ctrl.PHONE_CTRL_SCREEN_HIGHT/2+100)
        return 1
    else:
        return 0

#***********************************************************************
def hcgs_read_article_page():
    global read_count, read_time_totality
    phone_ctrl.phone_ctrl_screenshot()
    ret = combination_function.find_logo(HCGS_ARTICLE_LOGO, 980)
    if ret[0] == 0:
        debug_print.print_debug_log("not find artilce")
        return 0
    else:
        debug_print.print_debug_log("find artilce, reading")

    time.sleep(3)
    ok = False
    start_t = time.time()
    i = 0
    while ok == False:
        i += 1
        phone_ctrl.phone_ctrl_swipe_up_percent(50)
        time.sleep(1.5)
        phone_ctrl.phone_ctrl_swipe_down_percent(50)
        time.sleep(1.5)
        if i == 6 or i == 7 or i == 8:
            time.sleep(1)
            phone_ctrl.phone_ctrl_screenshot()
            ret = combination_function.find_logo(HCGS_ARTICLE_READ_FINISH_LOGO, 980)
            if ret[0] == 0:
                if i == 8:
                    ok = True
                    finish = 0
                    debug_print.print_debug_log("hcgs_read_article_page not finish")
            else:
                ok = True
                finish = 1
                read_count += 1
                debug_print.print_debug_log("hcgs_read_article_page finish")

    read_time_totality += time.time() - start_t
    return finish

#***********************************************************************
def hcgs_article_page_goback_home_page():
    phone_ctrl.phone_ctrl_go_back()




####################################################################################################################
"""
脚本参数 
1：激活设备编号（0~n）
2：手机APP图标分布 行
3：手机APP图标分布 列
4：app分身号
5：初始步骤
6：运行时间
"""
active_device_num =     int(sys.argv[1])
app_layout_line_def =   int(sys.argv[2])
app_layout_num_def =    int(sys.argv[3])
app_mirror_num =        int(sys.argv[4])
start_step =            int(sys.argv[5])
run_time =              int(sys.argv[6])

STEP_OPEN_APP = 0
STEP_APP_INIT = 1
STEP_HOME_TO_ARTICLE = 2
STEP_READ_ARTICLE = 3
STEP_BACK_TO_HOME = 4

combination_function.phone_init(active_device_num, app_layout_line_def, app_layout_num_def)
step = start_step
start_tine = time.time()
debug_print.print_debug_log("start hcgs " + str(app_mirror_num) + " at "+ str(datetime.datetime.now()))
ok = False
while ok == False:
    if step == STEP_OPEN_APP:
        debug_print.print_debug_log("STEP_OPEN_APP")
        phone_ctrl.phone_ctrl_open_app(3, app_mirror_num, 0)
        step = STEP_APP_INIT
        debug_print.print_debug_log("goto STEP_APP_INIT")

    if step == STEP_APP_INIT:
        time.sleep(20)
        debug_print.print_debug_log("STEP_APP_INIT")
        ret = hcgs_app_init()
        if ret == 1:
            step = STEP_HOME_TO_ARTICLE
            debug_print.print_debug_log("goto STEP_HOME_TO_ARTICLE")
        else:
            phone_ctrl.phone_ctrl_go_back()
            time.sleep(1)

    if step == STEP_HOME_TO_ARTICLE:
        time.sleep(1)
        debug_print.print_debug_log("STEP_HOME_TO_ARTICLE")
        ret = hcgs_home_page_gotot_article_page()
        if ret == 1:
            step = STEP_READ_ARTICLE
            debug_print.print_debug_log("goto STEP_READ_ARTICLE")
        else:
            step = STEP_APP_INIT
            debug_print.print_debug_log("goto STEP_APP_INIT")

    if step == STEP_READ_ARTICLE:
        time.sleep(3)
        debug_print.print_debug_log("STEP_READ_ARTICLE")
        ret = hcgs_read_article_page()
        step = STEP_BACK_TO_HOME
        debug_print.print_debug_log("goto STEP_BACK_TO_HOME")

    if step == STEP_BACK_TO_HOME:
        debug_print.print_debug_log("STEP_BACK_TO_HOME")
        hcgs_article_page_goback_home_page()
        step = STEP_HOME_TO_ARTICLE
        debug_print.print_debug_log("goto STEP_HOME_TO_ARTICLE")

    if read_count > 45:
        ok = True

    if read_time_totality > 1500:
        ok = True

    if time.time() - start_tine > run_time: #7200
        ok = True

    debug_print.print_debug_log("at "+ str(datetime.datetime.now()))
    debug_print.print_debug_log("runing time: " + str(int(time.time() - start_tine)))
    debug_print.print_debug_log("read_time_totality: " + str(int(read_time_totality)))
    debug_print.print_debug_log("read count: " + str(read_count))
debug_print.print_debug_log("end hcgs " + str(app_mirror_num) + " at "+ str(datetime.datetime.now()))
