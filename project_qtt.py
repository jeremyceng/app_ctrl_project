
import phone_ctrl
import image_handler
import debug_print
import combination_function
import time
import datetime
import sys


QTT_HOME_LOGO = "/qtt_im_home_logo.png"
QTT_HOME_AD_LOGO = "/qtt_im_home_ad.png"
QTT_ARTICLE_INDEX_LOGO = "/qtt_im_article_index_logo.png"
QTT_VEDIO_LOGO = "/qtt_im_vedio_logo.png"
QTT_ARTICLE_LOGO = "/qtt_im_article_logo.png"
QTT_EVERYDAT_MISSION_LOGO = "/qtt_im_everyday_mission_logo.png"
QTT_EVERYDAT_MISSION_AD_LOGO = "/qtt_im_everyday_mission_ad_logo.png"
QTT_SIGN_IN_LOGO = "/qtt_im_sign_in_logo.png"
QTT_GOTO_ARTICLE_LOGO = "/qtt_im_goto_article_logo.png"

QTT_NOT_VEDIO_LOGO = "/qtt_im_not_vedio_logo.png"
QTT_AWARD_LOGO = "/qtt_im_award_logo.png"

#***********************************************************************
#输出数据
read_count = 0
read_time_totality = 0
#***********************************************************************
def qtt_everyday_mission():
    phone_ctrl.phone_ctrl_screenshot()
    ret = combination_function.find_logo(QTT_EVERYDAT_MISSION_LOGO, 970)
    if ret[0] == 0:
        debug_print.print_debug_log("qtt_app_init not find everyday")
    else:
        debug_print.print_debug_log("qtt_app_init find everyday")
        phone_ctrl.phone_ctrl_tep_xy(ret[0], ret[1])
        time.sleep(2)
        phone_ctrl.phone_ctrl_screenshot()
        ret = combination_function.find_logo(QTT_EVERYDAT_MISSION_AD_LOGO, 970)
        if ret[0] == 0:
            debug_print.print_debug_log("qtt_app_init not find everyday AD")
        else:
            debug_print.print_debug_log("qtt_app_init find everyday AD")
            phone_ctrl.phone_ctrl_tep_xy(ret[0], ret[1])
            time.sleep(1)
            phone_ctrl.phone_ctrl_screenshot()
            ret = combination_function.find_logo(QTT_SIGN_IN_LOGO, 970)
            if ret[0] == 0:
                debug_print.print_debug_log("qtt_app_init not find sing_in")
            else:
                debug_print.print_debug_log("qtt_app_init find sing_in")
                phone_ctrl.phone_ctrl_tep_xy(ret[0], ret[1])
                time.sleep(10)

    phone_ctrl.phone_ctrl_screenshot()
    ret = combination_function.find_logo(QTT_GOTO_ARTICLE_LOGO, 970)
    if ret[0] == 0:
        debug_print.print_debug_log("qtt_app_init not find goto_article")
    else:
        debug_print.print_debug_log("qtt_app_init find goto_article")
        phone_ctrl.phone_ctrl_tep_xy(ret[0], ret[1])
        time.sleep(10)

#***********************************************************************
def qtt_app_init():
    """
    ok = False
    ad_count = 0
    while ok == False:
        phone_ctrl.phone_ctrl_screenshot()
        ret = combination_function.find_logo(QTT_HOME_AD_LOGO, 970)
        if ret[0] == 0:
            debug_print.print_debug_log("qtt_app_init not find ad")
            ok = True
        else:
            debug_print.print_debug_log("qtt_app_init find ad")
            phone_ctrl.phone_ctrl_go_back()
            time.sleep(1)
            ad_count += 1
            if ad_count > 5:
                ok = True
    """
    ok = False
    i = 0
    while ok == False:
        phone_ctrl.phone_ctrl_screenshot()
        ret = combination_function.find_logo(QTT_HOME_LOGO, 970)
        if ret[0] == 0:
            debug_print.print_debug_log("qtt_home_page_gotot_article_page not at home page")
            phone_ctrl.phone_ctrl_go_back()
            time.sleep(2)
            i += 1
            if i > 3:
                ok = True
        else:
            i = 0
            ok = True
    if i == 0:
        return 1
    else:
        return 0
#***********************************************************************
def qtt_home_page_gotot_article_page():
    phone_ctrl.phone_ctrl_screenshot()
    ret = combination_function.find_logo(QTT_HOME_LOGO, 970)
    if ret[0] == 0:
        debug_print.print_debug_log("qtt_home_page_gotot_article_page not at home")
        return 0
    else:
        debug_print.print_debug_log("qtt_home_page_gotot_article_page is at home")

    ret = combination_function.find_logo(QTT_AWARD_LOGO, 980)
    if ret[0] == 0:
        debug_print.print_debug_log("qtt_article_page_goback_home_page not find award")
    else:
        phone_ctrl.phone_ctrl_tep_xy(ret[0], ret[1])
        time.sleep(3)

    i = 0
    ok = False
    while ok == False:
        phone_ctrl.phone_ctrl_swipe_up_percent(80)
        time.sleep(1)
        phone_ctrl.phone_ctrl_screenshot()
        ret = combination_function.find_logo(QTT_ARTICLE_INDEX_LOGO, 970)
        ret_1 = combination_function.find_logo(QTT_VEDIO_LOGO, 970)
        if ret[0] == 0:
            debug_print.print_debug_log("qtt_home_page_gotot_article_page not find article")
            i += 1
            if i > 10:
                ok = True
        else:
            if abs(ret[1] - ret_1[1]) < 100:
                debug_print.print_debug_log("qtt_home_page_gotot_article_page find vedio")
            else:
                phone_ctrl.phone_ctrl_tep_xy(ret[0] - 100, ret[1])
                ok = True
                i = 0
    if i == 0:
        return 1
    else:
        return 0

#***********************************************************************
def qtt_read_article_page():
    global read_count, read_time_totality
    phone_ctrl.phone_ctrl_screenshot()
    ret = combination_function.find_logo(QTT_NOT_VEDIO_LOGO, 980)
    if ret[0] == 0:
        debug_print.print_debug_log("vedio, wrong")
        return 0
    else:
        debug_print.print_debug_log("not vedio,right")

    ret = combination_function.find_logo(QTT_ARTICLE_LOGO, 980)
    if ret[0] == 0:
        debug_print.print_debug_log("not find artilce")
        return 0
    else:
        debug_print.print_debug_log("find artilce, reading")

    time.sleep(3)
    ok = False
    start_t = time.time()
    while ok == False:
        i = 0
        while i < 20:
            i += 1
            phone_ctrl.phone_ctrl_key_down()
            time.sleep(0.5)
        i = 0
        while i < 20:
            i += 1
            phone_ctrl.phone_ctrl_key_up()
            time.sleep(0.5)
        debug_print.print_debug_log("read time: " + str(time.time() - start_t))
        if (time.time() - start_t) > 60:
            read_count += 1
            ok = True
    
    read_time_totality += time.time() - start_t
    return 1

#***********************************************************************
def qtt_article_page_goback_home_page():
    phone_ctrl.phone_ctrl_go_back()


####################################################################################################################
"""
脚本参数 
1：激活设备编号（0~n）
2：手机APP图标分布 行
3：手机APP图标分布 列
4：app分身号
5：初始步骤
6：运行时间
"""
active_device_num =     int(sys.argv[1])
app_layout_line_def =   int(sys.argv[2])
app_layout_num_def =    int(sys.argv[3])
app_mirror_num =        int(sys.argv[4])
start_step =            int(sys.argv[5])
run_time =              int(sys.argv[6])

STEP_OPEN_APP = 0
STEP_APP_INIT = 1
STEP_HOME_TO_ARTICLE = 2
STEP_READ_ARTICLE = 3
STEP_BACK_TO_HOME = 4
STEP_EVERYDAY_MISSION = 5

combination_function.phone_init(active_device_num, app_layout_line_def, app_layout_num_def)
step = start_step
start_tine = time.time()
debug_print.print_debug_log("start qtt " + str(app_mirror_num) + " at "+ str(datetime.datetime.now()))
ok = False
while ok == False:
    if step == STEP_OPEN_APP:
        phone_ctrl.phone_ctrl_open_app(3, app_mirror_num, 1)
        step = STEP_APP_INIT

    if step == STEP_APP_INIT:
        time.sleep(20)
        ret = qtt_app_init() 
        if ret == 1:
            step = STEP_EVERYDAY_MISSION
        else:
            step = STEP_OPEN_APP

    if step == STEP_EVERYDAY_MISSION:
#        qtt_everyday_mission()
        step = STEP_HOME_TO_ARTICLE

    if step == STEP_HOME_TO_ARTICLE:
        time.sleep(1)
        ret = qtt_home_page_gotot_article_page()
        if ret == 1:
            step = STEP_READ_ARTICLE
        else:
            step = STEP_APP_INIT

    if step == STEP_READ_ARTICLE:
        time.sleep(3)
        ret = qtt_read_article_page()
        step = STEP_BACK_TO_HOME

    if step == STEP_BACK_TO_HOME:
        qtt_article_page_goback_home_page()
        step = STEP_HOME_TO_ARTICLE
        
    if read_count > 46: #50
        ok = True

    if read_time_totality > 2600: #2500
        ok = True

    if time.time() - start_tine > run_time: #4500 3200
        ok = True

    debug_print.print_debug_log("at "+ str(datetime.datetime.now()))
    debug_print.print_debug_log("runing time: " + str(int(time.time() - start_tine)))
    debug_print.print_debug_log("read_time_totality: " + str(int(read_time_totality)))
    debug_print.print_debug_log("read count: " + str(read_count))
debug_print.print_debug_log("end qtt " + str(app_mirror_num) + " at "+ str(datetime.datetime.now()))
